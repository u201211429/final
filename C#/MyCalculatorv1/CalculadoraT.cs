﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCalculatorv1
{
    public class CalculadoraT
    {
        public String result(String op, String tb)
        {
            int iOp = 0;
            if (tb.Contains("+"))
            {
                iOp = tb.IndexOf("+");
            }
            else if (tb.Contains("-"))
            {
                iOp = tb.IndexOf("-");
            }
            else if (tb.Contains("*"))
            {
                iOp = tb.IndexOf("*");
            }
            else if (tb.Contains("/"))
            {
                iOp = tb.IndexOf("/");
            }
            else
            {
                //error
            }

            op = tb.Substring(iOp, 1);
            double op1 = Convert.ToDouble(tb.Substring(0, iOp));
            double op2 = Convert.ToDouble(tb.Substring(iOp + 1, tb.Length - iOp - 1));

            if (op == "+")
            {
                tb += "=" + (op1 + op2);
            }
            else if (op == "-")
            {
                tb += "=" + (op1 - op2);
            }
            else if (op == "*")
            {
                tb += "=" + (op1 * op2);
            }
            else
            {
                tb += "=" + (op1 / op2);
            }
            return tb;
        }
        public String resultException(String op, String tb)
        {
            try
            {
                return result(op, tb);
            }
            catch(Exception e)
            {
                return "Error!";
            }
        }
    }
}
