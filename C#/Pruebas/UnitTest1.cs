﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyCalculatorv1;

namespace Pruebas
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodSuma()
        {
            CalculadoraT TestCaculadora = new CalculadoraT();
            String r = TestCaculadora.result("+", "8+5");
            Assert.AreEqual(r, "8+5=13");
        }
        [TestMethod]
        public void TestMethodResta()
        {
            CalculadoraT TestCaculadora = new CalculadoraT();
            String r = TestCaculadora.result("-",  "8-5");
            Assert.AreEqual(r, "8-5=3");
        }
        [TestMethod]
        public void TestMethodMultiplicacion()
        {
            CalculadoraT TestCaculadora = new CalculadoraT();
            String r = TestCaculadora.result("*",  "8*5");
            Assert.AreEqual(r, "8*5=40");
        }
        [TestMethod]
        public void TestMethodDivicion()
        {
            CalculadoraT TestCaculadora = new CalculadoraT();
            String r = TestCaculadora.result("/",  "8/5");
            Assert.AreEqual(r, "8/5=1,6");
        }
        [TestMethod]
        public void TestMethodDivicionEntreCero()
        {
                CalculadoraT TestCaculadora = new CalculadoraT();
                String r = TestCaculadora.result("/", "8/0");
                Assert.AreEqual(r, "8/0=Infinito");
        }
        [TestMethod]
        public void TestMethodSumaEntreCaracterRaro()
        {
            CalculadoraT TestCaculadora = new CalculadoraT();
            String r = TestCaculadora.resultException("/", "¼+¼");
            Assert.AreEqual(r, "Error!");
        }
    }
}
